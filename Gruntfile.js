module.exports = function(grunt) {

  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    paths: {
      source: 'source',
      assets: 'assets',
      dist: 'dist',
    },
    // Configuration for clean
    clean: {
      options: {
        force: true
      },
      dist: {
        files: [{
          src: ['<%= paths.source %><%= paths.dist %>**/*']
        }]
      },
      tmp: {
        files: [{
          src: ['<%= paths.tmp %>**/*']
        }]
      }
    },


    //**************************************************************************
    // Configuration for PostCss
    postcss: {
      dev: {
        options: {
          map: {
            inline: false, // save all sourcemaps as separate files...
            annotation: '<%= paths.source %>/<%= paths.dist %>/' // ...to the specified directory
          },
          processors: [
            require("postcss-import")(),
            require("postcss-cssnext")({
              warnForDuplicates: false
            })
          ]
        },
        src: '<%= paths.source %>/<%= paths.assets %>/css/app.css',
        dest: '<%= paths.source %>/<%= paths.dist %>/app.css'
      },
      dist: {
        options: {
          map: {
            inline: false, // save all sourcemaps as separate files...
            annotation: '<%= paths.source %>/<%= paths.dist %>/' // ...to the specified directory
          },
          processors: [
            require("postcss-import")(),
            require("postcss-cssnext")({
              warnForDuplicates: false
            }),
            require("postcss-discard-comments")({
              removeAll: true
            }),
            require("cssnano")()
          ]
        },
        src: '<%= paths.source %>/<%= paths.assets %>/css/app.css',
        dest: '<%= paths.source %>/<%= paths.dist %>/app.css'
      }
    },


    //**************************************************************************
    // Configuration for JS Hint
    jshint: {
      options: {
        reporter: require('jshint-stylish'),
        'force': true,
        'asi': false,
        'bitwise': false,
        'boss': true,
        'browser': true,
        'curly': false,
        'eqeqeq': false,
        'eqnull': true,
        'evil': false,
        'forin': true,
        'immed': false,
        'indent': 4,
        'jquery': true,
        'laxbreak': true,
        'maxerr': 50,
        'newcap': false,
        'noarg': true,
        'noempty': false,
        'nonew': false,
        'nomen': false,
        'onevar': false,
        'plusplus': false,
        'regexp': false,
        'undef': false,
        'sub': true,
        'strict': false,
        'white': false,
      },
      ignore: {
        options: {
          '-W117': true,
          '-W030': true,
          "-W041": true
        },
        src: [
          '<%= paths.source %>/<%= paths.assets %>/js/**/*.js',
        ]
      }
    },


    copy: {
      dev: {
        files: [{
          src: ['<%= paths.source %>/<%= paths.assets %>/js/**/*.js'],
          dest: '<%= paths.source %>/<%= paths.dist %>',
          expand: true,
          flatten: true
        }]
      }
    },

    uglify: {
      js: {
        files: {
          '<%= paths.source %>/<%= paths.dist %>/app.js': ['<%= paths.source %>/<%= paths.dist %>/app.js']
        }
      }
    },

    //**************************************************************************
    // Configuration for watch task

    browserSync: {
      bsFiles: {
        src: [
					'<%= paths.source %>/*.html',
					'<%= paths.source %>/<%= paths.dist %>/*.css',
					'<%= paths.source %>/<%= paths.dist %>/*.js',
				]
      },
      options: {
				watchTask: true,
        reloadDelay: 1000,
				server: {
					baseDir: "source/"
				}
      }
    },

    //**************************************************************************
    // Configuration for watch task
    watch: {
      options: {
        spawn: false,
        debounceDelay: 250,
        dateFormat: function(time) {
          grunt.log.ok('The watch finished in ' + time + 'ms at ' + (new Date()).toString());
          grunt.log.subhead('Waiting for more changes...');
        }
      },
      css: {
        files: ['<%= paths.source %>/<%= paths.assets %>/css/**/*.css'],
        tasks: ['postcss:dev']
      },
      js: {
        files: ['<%= paths.source %>/<%= paths.assets %>/js/**/*.js'],
        tasks: ['jshint', 'copy:dev']
      },
    }


  });



  //**************************************************************************

  // Default -> Standard Build task
  grunt.registerTask('default', [
    'postcss:dev',
    'jshint',
    'copy:dev',
    'browserSync',
    'watch'
  ]);

  // Production -> Release task
  grunt.registerTask('release', [
    'postcss:dist',
    'jshint',
    'copy:dev',
    'jshint',
    'uglify',
  ]);



};
